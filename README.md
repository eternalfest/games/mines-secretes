# Les Mines Secrètes

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/mines-secretes.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/mines-secretes.git
```
