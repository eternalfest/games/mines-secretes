import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import vault.Vault;
import items.Items;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import atlas.Atlas;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	gameParams: GameParams,
	noNextLevel: NoNextLevel,
    items: Items,
    vault: Vault,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
	merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
