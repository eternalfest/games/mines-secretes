package items;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.IItem;
import custom_clips.CustomClips;

import items.specs.Livre;

@:build(patchman.Build.di())
class Items {
    @:diExport
    public var items(default, null): FrozenArray<IItem>;

    public function new(livre: Livre,
                        clips: CustomClips) {

        this.items = FrozenArray.of((new Livre(): IItem));
    }
}
