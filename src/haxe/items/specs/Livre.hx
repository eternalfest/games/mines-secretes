package items.specs;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import patchman.Ref;
import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.IItem;
import etwin.flash.filters.GlowFilter;

@:build(patchman.Build.di())
class Livre implements IItem {
    public static var ID_LIVRE: Int = 107;

    public var id(default, null): Int = 118;
    public var sprite(default, null): Null<String> = null;

    public function new() {}

    public function skin(mc: etwin.flash.MovieClip, subId: Null<Int>): Void {
        mc.gotoAndStop("" + (ID_LIVRE + 1));
        untyped mc.alpha = mc._alpha = 50;

        var glowFilter: etwin.flash.filters.GlowFilter = new GlowFilter();
        glowFilter.color = 6750207;
        glowFilter.quality = 3;
        glowFilter.strength = 100;
        glowFilter.blurX = 2;
        glowFilter.blurY = 2;
        glowFilter.alpha = 0.8;

        mc.filters = [glowFilter];
    }

    public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
        for (i in 0...5) {
            var v67 = hf.entity.item.ScoreItem.attach(specMan.game, item.x, item.y, 0, 9);
            v67.moveFrom(item, 8);
        }
    }

    public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    }
}
